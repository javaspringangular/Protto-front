FROM nginx:1.21.6

ARG SERVER_NAME="localhost"

RUN mkdir -p /etc/nginx/ssl

RUN apt-get update \
    && apt-get install openssl

RUN openssl req -subj "/CN=$SERVER_NAME" -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/nginx/ssl/localhost.key -out /etc/nginx/ssl/localhost.crt

EXPOSE 443
ENTRYPOINT ["nginx", "-g", "daemon off;"]
