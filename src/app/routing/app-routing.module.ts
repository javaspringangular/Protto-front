import {RouterModule, Routes} from "@angular/router";
import {LoginComponent} from "../gui/login/login.component";
import {NgModule} from "@angular/core";
import {HomeComponent} from "../gui/home/home.component";
import {AuthGuard} from "../guards/auth-guard/auhth.guard";
import {RegisterComponent} from "../gui/register/register.component";
import {ResetPasswordComponent} from "../gui/reset-password/reset-password.component";
import {DepositComponent} from "../gui/deposit/deposit.component";
import {HistoryComponent} from "../gui/history/history.component";
import {PdfViewComponent} from "../gui/pdf-view/pdf-view.component";

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'home', component: HomeComponent,
    canActivate: [AuthGuard]
  },
  {path: 'deposit', component: DepositComponent,
    canActivate: [AuthGuard]
  },
  { path: 'history', component: HistoryComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'pdfView/:idReport', component: PdfViewComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'resetPassword', component: ResetPasswordComponent },
  { path: '**', redirectTo: 'login' },
  { path: '*', redirectTo: 'login' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}


