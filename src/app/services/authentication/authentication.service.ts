import {environment} from "../../environment/dev/environment";
import {HttpClient} from "@angular/common/http";
import {Observable, tap} from "rxjs";
import {AuthenticationRequest, ResetPasswordRequest} from "../../model/model";
import jwt_decode from 'jwt-decode';
import {Injectable} from "@angular/core";

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  env = environment.apiBaseUrl;
  // @ts-ignore
  user: string;
  readonly userStorageKey = 'user';
  readonly adminKey = 'ADMIN';
  readonly clientKey = 'CLIENT';

  constructor(private readonly http: HttpClient) {}
  logout() {
    this.user = '';
    this.deleteToken();
  }

  login(user: AuthenticationRequest): Observable<any> {
    return this.http.post<string>(this.env + 'auth/login', user).pipe(
      tap((response: any) => {
        if (response) {
          this.user = response.token as string;
          this.setLoggedUser();
        }
      })
    );
  }

  isLoggedIn() {
    return sessionStorage.getItem(this.userStorageKey) !== null;
  }

  getDecodedAccessToken(token: null | string): any {
    return jwt_decode(<string>token);
  }

  getToken() {
    if (!sessionStorage.getItem(this.userStorageKey)) {
      return null;
    }

    return sessionStorage.getItem(this.userStorageKey);
  }

  private deleteToken() {
    sessionStorage.removeItem(this.userStorageKey)
  }

  private setLoggedUser() {
    sessionStorage.setItem(this.userStorageKey, this.user);
  }

  resetPassword(resetPasswordRequest: ResetPasswordRequest) {
    return this.http.post(this.env + 'auth/resetPassword', resetPasswordRequest);
  }

  getRoles() {
    if (!sessionStorage.getItem(this.userStorageKey)) {
      return null;
    }

    return this.getDecodedAccessToken(
      sessionStorage.getItem(this.userStorageKey)
    ).roles;
  }

  isAdmin() {
    if (!this.getRoles()) {
      return false;
    }
    return this.getRoles()[0] === this.adminKey;
  }

  isClient() {
    if (!this.getRoles()) {
      return false;
    }
    return this.getRoles()[0] === this.clientKey;
  }

  getLoggedUsername() {
    if (!sessionStorage.getItem(this.userStorageKey)) {
      return null;
    }

    return this.getDecodedAccessToken(
      sessionStorage.getItem(this.userStorageKey)
    ).sub;
  }
}
