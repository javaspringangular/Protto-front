import {Router} from "@angular/router";
import {Injectable} from "@angular/core";


@Injectable({
  providedIn: 'root'
})
export class NavigationService {

  constructor(private readonly router: Router) {
  }

  navigate(route: string, noSave?: boolean) {
    if (!noSave) {
      this.router.navigate([route], { state: { skip: true } });
    }

    this.router.navigate([route]);
  }

}
