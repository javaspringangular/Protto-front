import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {UserCreateRequest} from "../../model/model";
import {environment} from "../../environment/dev/environment";

@Injectable({
  providedIn: 'root'
})
export class RegistrationService {

  env = environment.apiBaseUrl;
  constructor(private readonly http: HttpClient) {}
  register(user: UserCreateRequest): Observable<any> {
    return this.http.post<number>(this.env + 'user/create', user);
  }

}
