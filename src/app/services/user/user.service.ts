import {environment} from "../../environment/dev/environment";
import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {UserInfo} from "../../model/model";

@Injectable({
  providedIn: 'root'
})
export class UserService {
  env = environment.apiBaseUrl + 'user';

  constructor(private readonly http: HttpClient) {}
  getUserInfo() {
    return this.http.get<UserInfo>(this.env + '/info');
  }
}
