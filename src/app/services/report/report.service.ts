import {Injectable} from "@angular/core";
import {environment} from "../../environment/dev/environment";
import {HttpClient} from "@angular/common/http";
import {DateRequest, Report, ReportCreateRequest} from "../../model/model";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ReportService {
  env = environment.apiBaseUrl + 'report';
  constructor(private readonly http: HttpClient) {}

  delete(id: number) {
    return this.http.delete(this.env + "/remove/" + id)
  }

  getMyReportsBySpecificDate(request: DateRequest): Observable<any> {
    return this.http.post<Report[]>(this.env + '/myReports' , request)
  }

  createReportFromInput(request: ReportCreateRequest): Observable<number> {
    return this.http.post<number>(this.env + '/create' , request);
  }

  uploadReport(request: FormData) {
    return this.http.post<number>(this.env + '/uploadReport' , request);
  }

  getPdf(idReport: any) {
    return this.http.get(this.env + '/pdf/' + idReport, {
      responseType: 'blob',
      observe: 'response'
    });
  }
}

