import {AuthenticationService} from "../authentication/authentication.service";
import {Router} from "@angular/router";
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable} from "rxjs";
import {Injectable} from "@angular/core";

const TOKEN_HEADER_KEY = 'Authorization';

@Injectable()
export class HttpInterceptorService implements HttpInterceptor {
  constructor(
    private authenticationService: AuthenticationService,
    private router: Router,
  ) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    let authReq = req;
    const token = this.authenticationService.getToken();
    if (token != null) {
      authReq = req.clone({
        headers: req.headers.set(TOKEN_HEADER_KEY, 'Bearer ' + token)
      });
    }
    if (token != null && !this.isTokenValid()) {
      this.authenticationService.logout();
      this.router.navigate(['/login']);
    }
    return next
      .handle(authReq);
  }

  isTokenValid() {
    const now = Date.now();
    const tokenExpirationDate = this.authenticationService.getDecodedAccessToken(this.authenticationService.getToken()).exp*1000;
    return now <= tokenExpirationDate;
  }
}
