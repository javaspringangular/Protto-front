import { MatSnackBar } from '@angular/material/snack-bar';
import {Injectable} from "@angular/core";

@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  constructor(private readonly snackBar: MatSnackBar) {
  }

  showSuccess(msg: string, action: string) {
    this.snackBar.open(msg, action, {
        duration: 2000,
        panelClass: ['green-snackbar']
    });
  }
  showDanger(msg: string, action: string) {
    this.snackBar.open(msg, action, {
        duration: 2000,
        panelClass: ['red-snackbar']
    });
  }
  showInfo(msg: string, action: string) {
    this.snackBar.open(msg, action, {
        duration: 2000,
        panelClass: ['yellow-snackbar']
    });
  }
}
