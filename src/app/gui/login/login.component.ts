import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute} from "@angular/router";
import {AuthenticationService} from "../../services/authentication/authentication.service";
import {NavigationService} from "../../services/nagivation/navigation.service";
import {AuthenticationRequest} from "../../model/model";
import {NotificationService} from "../../services/notification/notification.service";
import {ErrorCode} from "../../model/errorCode";
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  hide = true;
  constructor(
    private formBuilder: FormBuilder,
    private navigationService: NavigationService,
    private readonly activatedRoute: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private notificationService: NotificationService
  ) {
    if (this.authenticationService.isLoggedIn()) {
      this.navigationService.navigate('/');
    }
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  connexion() {
    if(this.loginForm.controls['username'].invalid)
      return;
    if(this.loginForm.controls['password'].invalid)
      return;

    const user: AuthenticationRequest = {
      username: this.loginForm.value.username,
      password: this.loginForm.value.password
    }

    this.authenticationService
      .login(user)
      .subscribe( {
        next: () => {
          this.notificationService.showSuccess('Authentication success', 'Close');
          this.navigationService.navigate('/home');
        },
        error: (err) => {
          this.notificationService.showDanger(ErrorCode.errorCodeToMessage(err.error.errorCode), 'Retry');
          this.loginForm.reset();
        }
      });
  }

  isSubmitDisabled() {
    return this.loginForm.invalid;
  }
}
