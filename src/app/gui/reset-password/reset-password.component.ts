import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {NavigationService} from "../../services/nagivation/navigation.service";
import {ActivatedRoute} from "@angular/router";
import {NotificationService} from "../../services/notification/notification.service";
import {AuthenticationService} from "../../services/authentication/authentication.service";
import {ResetPasswordRequest} from "../../model/model";
import {ErrorCode} from "../../model/errorCode";
import {Patterns} from "../../util/Patterns";

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit{

  resetForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private navigationService: NavigationService,
    private readonly activatedRoute: ActivatedRoute,
    private notificationService: NotificationService,
    private authenticationService: AuthenticationService

  ) {
  }

  ngOnInit() {
    this.resetForm = this.formBuilder.group({
      email: ['', [
        Validators.required,
        Validators.pattern(Patterns.EMAIL)]
      ],
      phoneNumber: ['', [
        Validators.required,
        Validators.pattern(Patterns.PHONENUMBER)
        ]
      ]
    });
  }

  resetPassword() {
    if(this.resetForm.controls['email'].invalid) {
      return;
    }
    if(this.resetForm.controls['phoneNumber'].invalid) {
      return;
    }

    const resetPasswordRequest: ResetPasswordRequest = {
      login: this.resetForm.value.email,
      phoneNumber: this.resetForm.value.phoneNumber,
    };

    this.authenticationService
      .resetPassword(resetPasswordRequest)
      .subscribe( {
        next: () => {
          this.notificationService.showSuccess('Request has been send', 'Ok');
          this.navigationService.navigate('login');
        },
        error: (err) => {
          this.notificationService.showDanger(ErrorCode.errorCodeToMessage(err.error.errorCode), '\u2713');
          this.resetForm.reset();
        }
      });
  }

  onlyNumberKey(event: { charCode: number; }) {
    return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
  }
}
