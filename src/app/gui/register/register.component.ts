import { Component, OnInit } from '@angular/core';
import {AbstractControlOptions, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MustMatch} from "../../util/validator";
import {RegistrationService} from "../../services/registration/registration.service";
import {NavigationService} from "../../services/nagivation/navigation.service";
import {Roles, UserCreateRequest} from "../../model/model";
import {NotificationService} from "../../services/notification/notification.service";
import {ErrorCode} from "../../model/errorCode";
import {Patterns} from "../../util/Patterns";

@Component({ selector: 'register', templateUrl: 'register.component.html' })
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  roles: Roles[] = [
    {
      value: '@consultant-solutec.fr',
      viewValue: '@consultant-solutec.fr'
    },
    {
      value: '@solutec.fr',
      viewValue: '@solutec.fr'
    },
  ];

  constructor(
    private formBuilder: FormBuilder,
    private readonly registrationService: RegistrationService,
    private readonly navigationService: NavigationService,
    private readonly  notificationService: NotificationService
  ) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', [
        Validators.required,
        Validators.pattern(Patterns.LEFT_EMAIL)]
      ],
      role: [null, Validators.required],
      password: ['', [
        Validators.required,
        Validators.pattern(Patterns.PASSWORD)]
      ],
      confirmPassword: ['', Validators.required]
    }, {
      validator: MustMatch('password', 'confirmPassword')
    } as AbstractControlOptions);
  }

  register() {
    if(this.registerForm.controls['lastName'].invalid) {
      return;
    }
    if(this.registerForm.controls['firstName'].invalid) {
      return;
    }
    if(this.registerForm.controls['email'].invalid) {
      return;
    }
    if(this.registerForm.controls['password'].invalid) {
      return;
    }
    if(this.registerForm.controls['role'].invalid){
      return;
    }

    const userToRegister: UserCreateRequest = {
      lastName: this.registerForm.value.lastName,
      firstName: this.registerForm.value.firstName,
      login: this.registerForm.value.email + this.registerForm.value.role,
      password: this.registerForm.value.password
    };

    this.registrationService
      .register(userToRegister)
      .subscribe( {
        next: () => {
          this.notificationService.showSuccess('Registration Success, check your mails', 'Ok');
          this.navigationService.navigate('login');
        },
        error: (err) => {
          this.notificationService.showDanger(ErrorCode.errorCodeToMessage(err.error.errorCode), '\u2713');
          this.registerForm.reset();
        }
      });
  }

  isSubmitDisabled() {
    return this.registerForm.invalid;
  }
}
