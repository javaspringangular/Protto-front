import {Component, OnInit, ViewChild} from '@angular/core';
import {ReportService} from "../../services/report/report.service";
import {AuthenticationService} from "../../services/authentication/authentication.service";
import {MatTableDataSource} from "@angular/material/table";
import {NotificationService} from "../../services/notification/notification.service";
import {NavigationService} from "../../services/nagivation/navigation.service";
import {ErrorCode} from "../../model/errorCode";
import {MatDatepicker} from "@angular/material/datepicker";
import {FormControl} from "@angular/forms";
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from "@angular/material/core";
import {MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS} from '@angular/material-moment-adapter';
import {DateRequest} from "../../model/model";
import {DateUtil} from "../../util/DateUtil";
import {MatSort} from "@angular/material/sort";
import {DATE_FORMAT_MMYYYY} from "../../model/Constant";

//@ts-ignore
import {default as _rollupMoment, Moment} from "moment/moment";
import * as _moment from "moment/moment";
const moment = _rollupMoment || _moment;

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss'],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },
    {
      provide: MAT_DATE_FORMATS,
      useValue: DATE_FORMAT_MMYYYY
    },
  ],
})
export class HistoryComponent implements OnInit {

  constructor(private readonly reportService: ReportService,
              private readonly authenticationService: AuthenticationService,
              private readonly notificationService: NotificationService,
              private readonly navigationService: NavigationService) {
  }

  // @ts-ignore
  @ViewChild(MatSort) set matSort(sort: Matsort) {
    this.data_table.sort = sort;
  }
  // @ts-ignore
  data_table: MatTableDataSource<any> = new MatTableDataSource([]);
  displayedColumns: string[] = ['name', 'date', 'depositDate', 'view', 'delete'];
  displayedColumnsName: string[] = ['Nom', 'Date', 'Date de depot'];
  dateSelected: boolean = false;

  date: FormControl<any>;
  normalizedDate: string;
  year: number = -1;
  month: number = -1;

  ngOnInit() {
    this.initDateDefault();
    this.getReportsOfDate();
  }

  onView(idReport: number) {
    this.navigationService.navigate('/pdfView/' + idReport);
  }

  onDelete(id: number) {
    this.reportService
      .delete(id)
      .pipe()
      .subscribe({
        next: () => {
          this.getReportsOfDate();
        },
        error: err => {
          this.notificationService.showDanger(ErrorCode.errorCodeToMessage(err.error.errorCode), 'Ok');
        }
      });
  }

  initDateDefault() {
    this.date = new FormControl(moment());
    //init date with current date by default
    this.normalizedDate = DateUtil.normalize(moment().month() + 1, moment().year());
    this.year = moment().year();
    this.month = moment().month() + 1;
  }

  upload() {
    this.navigationService.navigate('/deposit')
  }

  getReportsOfDate() {
    if(this.year === (-1))
      return;
    if(this.month === (-1))
      return;

    const request: DateRequest = {
      year: this.year,
      month: this.month
    };

    this.reportService
      .getMyReportsBySpecificDate(request)
      .pipe()
      .subscribe({
        next: (res) => {
          this.data_table.data = res;
          this.dateSelected = true;
          this.notificationService.showSuccess('Reports loaded successfully', 'Ok');
        },
        error: err => {
          this.notificationService.showDanger(ErrorCode.errorCodeToMessage(err.error.errorCode), 'Ok');
        }
      });
  }

  chosenYearHandler(normalizedYear: Moment) {
    const ctrlValue = this.date.value;
    ctrlValue.year(normalizedYear.year());
    this.date.setValue(ctrlValue);

    this.year = normalizedYear.year();
  }

  chosenMonthHandler(normalizedMonth: Moment, datepicker: MatDatepicker<Moment>) {
    const ctrlValue = this.date.value;
    ctrlValue.month(normalizedMonth.month());
    this.date.setValue(ctrlValue);

    this.month = normalizedMonth.month() + 1;
    datepicker.close();
    this.normalizedDate = DateUtil.normalize(this.month, this.year);
    this.getReportsOfDate();
  }
}
