import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NavigationService} from "../../services/nagivation/navigation.service";
import {ReportCreateRequest} from "../../model/model";
import {NotificationService} from "../../services/notification/notification.service";
import {ErrorCode} from "../../model/errorCode";
import {MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {ReportService} from "../../services/report/report.service";
// @ts-ignore
import {default as _rollupMoment} from "moment/moment";
import * as _moment from "moment/moment";
import {DATE_FORMAT_DDMMYYYY} from "../../model/Constant";

const moment = _rollupMoment || _moment;

@Component({
  selector: 'app-deposit',
  templateUrl: './deposit.component.html',
  styleUrls: ['./deposit.component.scss'],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS],
    },
    {
      provide: MAT_DATE_FORMATS,
      useValue: DATE_FORMAT_DDMMYYYY
    },
    {
      provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: { useUtc: true }
    },
  ]
})
export class DepositComponent implements OnInit{

  depositForm: FormGroup;
  date: FormControl<any>;

  files: File[] = [];
  fileDropped: boolean = false;

  accordionExpanded: boolean = false;
  accordionDisabled: boolean = false;

  constructor(
    private readonly reportService: ReportService,
    private formBuilder: FormBuilder,
    private readonly navigationService: NavigationService,
    private readonly notificationService: NotificationService
  ){ }

  ngOnInit() {
    this.depositForm = this.formBuilder.group({
      comExchanges : ['',Validators.required],
      selfStudies : ['',Validators.required],
      projects : ['',Validators.required]
    });
    this.date = new FormControl(moment());
  }

  onSelect(event: { addedFiles: any; }) {
    if(this.files && this.files.length <= 1) {
      this.onRemove(this.files[0]);
    }
    this.files.push(...event.addedFiles);
    this.fileDropped = true;
    this.accordionDisabled = true;
  }

  onRemove(event: File) {
    this.files.splice(this.files.indexOf(event), 1);
    this.fileDropped = false;
    this.accordionDisabled = false;
  }

  deposit(){
    if(this.depositForm.controls['comExchanges'].invalid) {
      return;
    }
    if(this.depositForm.controls['selfStudies'].invalid) {
      return;
    }
    if(this.depositForm.controls['projects'].invalid) {
      return;
    }

    const createReport: ReportCreateRequest = {
      reportDate: this.date.value,
      comExchanges: this.depositForm.value.comExchanges,
      selfStudies: this.depositForm.value.selfStudies,
      projects: this.depositForm.value.projects
    };

    this.reportService
      .createReportFromInput(createReport)
      .pipe()
      .subscribe({
        next: () => {
          this.navigationService.navigate('/history');
          this.notificationService.showSuccess('Report created successfully', 'Ok');
        },
        error: err => {
          this.notificationService.showDanger(ErrorCode.errorCodeToMessage(err.error.errorCode),'Ok');
        }
      });
  }

  uploadFile(){
    const file: File = this.files[0];
    const formData: FormData = new FormData();
    formData.append('file', file);

    this.reportService.uploadReport(formData)
      .pipe()
      .subscribe(
        {
          next: () => {
            this.notificationService.showSuccess('Report uploaded successfully', 'Ok');
          },
          error: (err) => {
            this.notificationService.showDanger(ErrorCode.errorCodeToMessage(err.error.errorCode), 'Ok');
          }
        });
  }

  isSubmitDisabled() {
    return this.depositForm.invalid;
  }

  isSubmitDropZoneDisabled() {
    return this.fileDropped;
  }

  isCancelDisabled() {
    if(this.depositForm.controls['comExchanges'].touched)
      return false;
    if(this.depositForm.controls['selfStudies'].touched)
      return false;
    return !this.depositForm.controls['projects'].touched;
  }

  toggleAccordionOpen() {
    return this.accordionExpanded;
  }

  openAccordion() {
    this.accordionExpanded = true;
    this.toggleAccordionOpen();
  }

  closeAccordion() {
    this.accordionExpanded = false;
    this.toggleAccordionOpen();
  }

  resetManualForm() {
    this.closeAccordion();
    // @ts-ignore
    this.depositForm.get('comExchanges').reset();
    // @ts-ignore
    this.depositForm.get('comExchanges').setValidators([Validators.required]);
    // @ts-ignore
    this.depositForm.get('comExchanges').updateValueAndValidity();
    // @ts-ignore
    this.depositForm.get('selfStudies').reset();
    // @ts-ignore
    this.depositForm.get('selfStudies').setValidators([Validators.required]);
    // @ts-ignore
    this.depositForm.get('selfStudies').updateValueAndValidity();
    // @ts-ignore
    this.depositForm.get('projects').reset();
    // @ts-ignore
    this.depositForm.get('projects').setValidators([Validators.required]);
    // @ts-ignore
    this.depositForm.get('projects').updateValueAndValidity();
  }
}




