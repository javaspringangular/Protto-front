import { Component } from '@angular/core';
import { AuthenticationService } from "../../services/authentication/authentication.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {

  constructor(private readonly authenticationService: AuthenticationService,
              ){
  }

  isAdmin() {
    return this.authenticationService.isAdmin();
  }

  isClient(){
    return this.authenticationService.isClient();
  }
}
