import {Component, OnInit} from '@angular/core';
import {ReportService} from "../../services/report/report.service";
import {NotificationService} from "../../services/notification/notification.service";
import {ActivatedRoute} from "@angular/router";
import {NavigationService} from "../../services/nagivation/navigation.service";
import {ErrorCode} from "../../model/errorCode";

@Component({
  selector: 'app-pdf-view',
  templateUrl: './pdf-view.component.html',
  styleUrls: ['./pdf-view.component.scss']
})
export class PdfViewComponent implements OnInit{

  reportId: number | undefined;
  isVisualize: boolean = false;
  blobFile: Blob;
  private fileName: string;

  constructor(private readonly reportService: ReportService,
              private readonly notificationService: NotificationService,
              private readonly activatedRoute: ActivatedRoute,
              private readonly navigationService: NavigationService){
    this.reportId = activatedRoute.snapshot.params['idReport'];
    if (this.reportId && isNaN(this.reportId)) {
      this.notificationService.showInfo('Id is required', 'Ok');
      this.reportId = undefined;
      this.navigationService.navigate('/home');
    }
    if (this.reportId && this.reportId < 1) {
      this.notificationService.showInfo("Id must be positive", 'Ok');
      this.reportId = undefined;
      this.navigationService.navigate('/home');
    }
  }

  ngOnInit() {
    this.getPdfReport(this.reportId);
  }


  getPdfReport(reportId: any) {
    this.reportService.getPdf(reportId)
      .pipe()
      .subscribe({
        next: (res) => {
          //@ts-ignore
          this.blobFile = res.body;
          this.isVisualize = true;
          this.fileName = <string>res.headers.get("content-disposition")?.split(';')[1].split('filename')[1].split('=')[1].trim();
        },
        error: (err) => {
          this.notificationService.showDanger(ErrorCode.errorCodeToMessage(err.error.errorCode), 'Ok');
          this.navigationService.navigate('/home');
        }
      });
  }

  exitView() {
    this.navigationService.navigate('/history');
  }

  downloadPdf() {
    let downloadLink = document.createElement('a');
    let data = [];
    data.push(this.blobFile);
    downloadLink.href = window.URL.createObjectURL(new Blob(data, {type: 'application/pdf'}));
    downloadLink.download = this.fileName;
    downloadLink.setAttribute('style', 'display: none');
    document.body.appendChild(downloadLink);
    downloadLink.click();
    window.URL.revokeObjectURL(downloadLink.href);
    downloadLink.remove();
  }

}
