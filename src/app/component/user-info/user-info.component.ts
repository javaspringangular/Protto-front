import {Component, OnInit} from '@angular/core';
import { NotificationService } from "../../services/notification/notification.service";
import { ErrorCode } from "../../model/errorCode";
import { UserInfo } from "../../model/model";
import { UserService } from "../../services/user/user.service"

@Component({
  selector: 'user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.scss']
})
export class UserInfoComponent implements OnInit {

  userInfo : UserInfo;

  constructor(private readonly notificationService: NotificationService,
              private readonly userService : UserService){
  }

  ngOnInit() {
    this.getUserInfo();
  }

  getUserInfo(){
    this.userService.getUserInfo()
      .pipe()
      .subscribe({
        next: (res) => {
          this.userInfo = res;
        },
        error: (err) => {
          this.notificationService.showDanger(ErrorCode.errorCodeToMessage(err.error.errorCode),'');
        }
      });
  }
}
