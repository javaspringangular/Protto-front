import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'accordion',
  templateUrl: './accordion.component.html',
  styleUrls: ['./accordion.component.scss']
})
export class AccordionComponent {
  @Input() class: string = '';
  @Input() disabled: boolean = false;
  @Input() accordionTitle: string = '';
  @Input() accordionDescription: string = '';
  @Input() expanded: boolean = false;

  @Output() opened: EventEmitter<any> = new EventEmitter<any>();
  @Output() closed: EventEmitter<any> = new EventEmitter<any>();

  isDisabled() {
    return this.disabled;
  }

  isExpanded() {
    return this.expanded;
  }

  close() {
    this.expanded = false;
    this.closed.emit(this.expanded);
  }

  open() {
    this.expanded = true;
    this.opened.emit(this.expanded);
  }
}

