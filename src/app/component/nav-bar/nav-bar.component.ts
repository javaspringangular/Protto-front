import {Component, Input} from '@angular/core';
import {AuthenticationService} from "../../services/authentication/authentication.service";
import {NavigationService} from "../../services/nagivation/navigation.service";
import {Router} from "@angular/router";

@Component({
  selector: 'nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent {
  @Input() listButton: any[] = [];

  constructor(
    private readonly authenticationService: AuthenticationService,
    private readonly navigationService: NavigationService,
    private readonly router: Router
  ) {
  }

  listButtonAdmin = [
    { name: 'Administration', route: 'admin'}
  ];
  listButtonClient = [
    { name: 'Deposit', route: 'deposit' },
    { name: 'History', route: 'history' }
  ];

  getUsername() {
    return this.authenticationService.getLoggedUsername().split('@')[0];
  }

  isLoggedIn() {
    return this.authenticationService.isLoggedIn();
  }

  logOut() {
    this.authenticationService.logout();
    this.navigationService.navigate('login');
  }

  routerLink(href: string) {
    this.router.navigateByUrl(href);
  }

  isAdmin() {
    return this.authenticationService.isAdmin();
  }

  isClient() {
    return this.authenticationService.isClient();
  }
}
