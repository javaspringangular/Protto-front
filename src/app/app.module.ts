import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { RouterOutlet } from "@angular/router";
import { ReactiveFormsModule } from "@angular/forms";
import { NgModule } from "@angular/core";
import { HttpInterceptorService } from "./services/interceptor/request-interceptor";
import {
  HTTP_INTERCEPTORS,
  HttpClientModule
} from '@angular/common/http';
import { AppRoutingModule } from "./routing/app-routing.module";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from "@angular/material/button";
import { MatInputModule } from "@angular/material/input";
import { MatSnackBarModule } from "@angular/material/snack-bar";
import { MatListModule } from "@angular/material/list";
import { ResetPasswordComponent } from './gui/reset-password/reset-password.component';
import { HomeComponent } from './gui/home/home.component';
import { RegisterComponent } from './gui/register/register.component';
import { LoginComponent } from './gui/login/login.component';
import { NavBarComponent } from "./component/nav-bar/nav-bar.component";
import { MatIconModule } from "@angular/material/icon";
import { MatSelectModule } from "@angular/material/select";
import { TitleCaseWordPipe } from "./util/TitleCaseWord";
import { DepositComponent } from './gui/deposit/deposit.component';
import { HistoryComponent } from './gui/history/history.component';
import { MatTableModule } from "@angular/material/table";
import { MatDatepickerModule } from "@angular/material/datepicker";
import { MatTooltipModule } from "@angular/material/tooltip";
import { MatSortModule } from "@angular/material/sort";
import { NgxDropzoneModule } from 'ngx-dropzone';
import { AccordionComponent } from './component/accordion/accordion.component';
import { MatExpansionModule } from "@angular/material/expansion";
import { PdfViewComponent } from './gui/pdf-view/pdf-view.component';
import { NgxExtendedPdfViewerModule } from 'ngx-extended-pdf-viewer';
import { UserInfoComponent } from './component/user-info/user-info.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    RegisterComponent,
    ResetPasswordComponent,
    RegisterComponent,
    NavBarComponent,
    TitleCaseWordPipe,
    DepositComponent,
    HistoryComponent,
    AccordionComponent,
    PdfViewComponent,
    UserInfoComponent
  ],
  imports: [
    BrowserModule,
    RouterOutlet,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatInputModule,
    MatSnackBarModule,
    MatListModule,
    MatIconModule,
    MatSelectModule,
    NgxDropzoneModule,
    MatTableModule,
    MatDatepickerModule,
    MatTooltipModule,
    MatSortModule,
    MatExpansionModule,
    NgxExtendedPdfViewerModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpInterceptorService,
      multi: true
    }
  ],
  bootstrap: [AppComponent],
  exports: [TitleCaseWordPipe]
})
export class AppModule { }
