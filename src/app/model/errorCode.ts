export class ErrorCode {
  static errorCodeToMessage(errorCode: string, opt?: any): string {
    switch (errorCode) {
      case 'login.bad.credential': {
        return 'Bad credentials information';
      }
      case 'user.already.exist': {
        return 'User is already registered';
      }
      case 'user.not.found': {
        return 'User ' + opt + ' not found';
      }
      case 'report.no.access': {
        return 'You dont have access to this report';
      }
      case 'report.no.rights': {
        return 'Admin cannot create weekly report';
      }
      case 'report.not.found': {
        return 'Report not found';
      }
      case 'user.login.pattern': {
        return 'Email must respect pattern';
      }
      case 'report.uploading.error': {
        return 'Error server when upload';
      }
      case 'pdf.generating.error': {
        return 'Error server when generate pdf';
      }
      case 'pdf.generating.error.user.login': {
        return 'You do not have the right to save a report if it does not include your login';
      }
      case 'parser.error.range': {
        return 'Error server parsing range key';
      }
      case 'parser.error.data': {
        return 'Error in your report, please respect template';
      }
      case 'pdf.uploading.error.date.format': {
        return 'Date format must be YYYY-MM-DD';
      }
      default: {
        return 'Error server';
      }
    }
  }
}
