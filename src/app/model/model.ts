export interface AuthenticationRequest {
  username: string;
  password: string;
}

export interface UserCreateRequest {

  firstName: string;
  lastName: string;
  login: string;
  password: string;
}

export interface DateRequest {
  year: number;
  month: number;
}

export interface Roles{
  value: string;
  viewValue: string;
}

export interface ResetPasswordRequest {
  login: string;
  phoneNumber: string;
}

export interface Report {
  id: number;
  name: string;
  date: Date;
  depositDate: Date;
}

export interface ReportCreateRequest {
  reportDate: Date;
  comExchanges: string;
  selfStudies: string;
  projects: string;
}

export interface UserInfo{
  firstName: string;
  lastName: string;
}
