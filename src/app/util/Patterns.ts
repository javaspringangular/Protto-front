
export class Patterns {
  public static readonly EMAIL =
    '^[a-z0-9._+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$';  //pattern d'email left@right.2/4d
  public static readonly LEFT_EMAIL =
    '^[A-z0-9._+-]{1,64}$'; // pattern gauche de l'email
  public static readonly PASSWORD =
    '^(?=.{8,}$)(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).*$'; //au moins une maj une min un chiffre avec 8 caractères minimum
  public static readonly PHONENUMBER =
    '^[0-9]{10}$' //pattern numéro de tel
}
